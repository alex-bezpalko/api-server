const httpStatus = require('http-status');
const _ = require('lodash');

const APIError = require('../helpers/APIError');
const { getUsers, saveUsers } = require('../repositories/user.repository');

class UserService {
  getList() {
    return (req, res, next) => {
      getUsers((err, list) => {
        if (err) {
          console.log(err);
          return next(err);
        }
        res.status(httpStatus.OK).json({
          data: list,
        });
      });
    }
  }

  getById() {
    return (req, res, next) => {
      const { id } = req.params;
      getUsers((err, list) => {
        if (err) {
          console.log(err);
          return next(err);
        }
        const result = _.filter(list, item => item.id == id);
        if (!result.length) {
          return next(new APIError(`user with id ${id} does not exist`, httpStatus.NOT_FOUND, true));
        }
        const response = result.length == 1 ? result[0] : result;
        res.status(httpStatus.OK).json({
          data: response,
        });
      });
    }
  }

  create() {
    return (req, res, next) => {
      const { ...user } = req.body;
      getUsers((err, list) => {
        if (err) {
          console.log(err);
          return next(err);
        }
        user.id = list.length + 1;
        const result = _.concat(list, user);
        const jsonString = JSON.stringify(result, null, 2);
        saveUsers(jsonString, err => {
          if (err) {
            console.log('Error writing file', err);
            return next(new APIError(`can't create user`, httpStatus.UNPROCESSABLE_ENTITY, true));
          }
          res.sendStatus(httpStatus.CREATED);
        });
      });
    }
  }

  update() {
    return (req, res, next) => {
      const { id } = req.params;
      const { ...user } = req.body;
      getUsers((err, list) => {
        if (err) {
          console.log(err);
          return next(err);
        }
        let findUser = false
        const result = _.map(list, item => {
          if (item.id == id) {
            findUser = true;
            return { ...item, ...user };
          }
          return item;
        });
        if (!findUser) {
          return next(new APIError(`user with id ${id} does not exist`, httpStatus.NOT_FOUND, true));
        }
        const jsonString = JSON.stringify(result, null, 2);
        saveUsers(jsonString, err => {
          if (err) {
            console.log('Error writing file', err);
            return next(new APIError(`can't update user`, httpStatus.UNPROCESSABLE_ENTITY, true));
          }
          res.sendStatus(httpStatus.OK);
        });
      });
    }
  }

  delete() {
    return (req, res, next) => {
      const { id } = req.params;
      getUsers((err, list) => {
        if (err) {
          console.log(err);
          return next(err);
        }
        const listLength = list.length;
        _.remove(list, item => item.id == id);
        if (listLength == list.length) {
          return next(new APIError(`user with id ${id} does not exist`, httpStatus.NOT_FOUND, true));
        }
        const jsonString = JSON.stringify(list, null, 2);
        saveUsers(jsonString, err => {
          if (err) {
            console.log('Error writing file', err);
            return next(new APIError(`can't delete user`, httpStatus.UNPROCESSABLE_ENTITY, true));
          }
          res.sendStatus(httpStatus.NO_CONTENT);
        });
      });
    }
  }
}

module.exports = UserService;