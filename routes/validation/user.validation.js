const Joi = require('joi');

const create = {
  body: {
    first_name: Joi.string().min(3).max(30).required(),
    last_name: Joi.string().min(3).max(30).required(),
    email: Joi.string().email({ minDomainSegments: 2 }).required(),
    gender: Joi.any().valid('Male', 'Female').required(),
    company_name: Joi.string().min(3).max(30).required(),
    job_title: Joi.string().min(3).max(30).required(),
  },
};

const update = {
  body: {
    first_name: Joi.string().min(3).max(30),
    last_name: Joi.string().min(3).max(30),
    email: Joi.string().email({ minDomainSegments: 2 }),
    gender: Joi.any().valid('Male', 'Female'),
    company_name: Joi.string().min(3).max(30),
    job_title: Joi.string().min(3).max(30),
  },
};

module.exports = {
  create,
  update,
};
