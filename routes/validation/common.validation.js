const Joi = require('joi');

const paramsId = {
  params: {
    id: Joi.number().integer().required(),
  },
};

module.exports = {
  paramsId,
};
