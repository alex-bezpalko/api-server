const express = require('express');
const validate = require('express-validation');

const router = express.Router();

const UserService = require('../services/user.service');
const rules = require('./validation/user.validation');
const { paramsId } = require('./validation/common.validation');
const stripMiddleware = require('../middlewares/stripBody');

const userService = new UserService();

/* GET users listing. */
router.get('/', userService.getList());

/* GET user by id. */
router.get('/:id', validate(paramsId), userService.getById());

/* CREATE user. */
router.post('/', stripMiddleware(rules.create), validate(rules.create), userService.create());

/* UPDATE user by id. */
router.put('/:id', stripMiddleware(rules.update), validate(paramsId), validate(rules.update), userService.update());

/* DELETE user by id. */
router.delete('/:id', validate(paramsId), userService.delete());

module.exports = router;
