const _ = require('lodash');

// remove not described fields in schema from request body

const stripMiddleware = schema => (req, res, next) => {
  if (!_.isObject(schema)) {
    throw new Error(`Invalid schema, expected 'object',  receive ${typeof schema}`);
  }
  const allowProperty = _.keys(schema.body);
  req.body = _.pickBy(req.body, (value, key) => allowProperty.includes(key));
  next();
};

module.exports = stripMiddleware;
