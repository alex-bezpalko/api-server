const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const httpStatus = require('http-status');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const errorConverterMiddleware = require('./middlewares/errorConverter');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRouter);
app.use('/user', usersRouter);

// if error is not an instanceOf APIError, convert it.
app.use(errorConverterMiddleware);

// error handler
app.use((err, req, res, next) =>
  res.status(err.status).json({
    message: err.isPublic ? err.message : httpStatus[err.status],
  }));

module.exports = app;
