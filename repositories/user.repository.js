const fs = require('fs');

const dataPath = './data/data.json';

const getUsers = callback => {
  fs.readFile(dataPath, (err, fileData) => {
    if (err) {
      return callback && callback(err);
    }
    try {
      const object = JSON.parse(fileData);
      return callback && callback(null, object);
    } catch (err) {
      return callback && callback(err);
    }
  })
}

const saveUsers = (data, callback) => {
  fs.writeFile(dataPath, data, err => {
    if (err) {
      return callback && callback(err);
    }
    return callback && callback();
  })
}

module.exports = {
  getUsers,
  saveUsers,
};